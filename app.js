class Person {
    constructor(name, address) {
        this.name = name;
        this.address = address;
    }
    sayHello() {
        console.log(
            `Hallo nama saya ${this.name} saya berasal dari ${this.address}`
        );
    }
}




class Security extends Person {
    static register(visitor = 'Anda', intent) {
        if (!(visitor instanceof Customer)) return alert(`${visitor} bukan customer kami`)
        let serviseAvailailabeList = ['withdraw', 'deposit', 'transfer', 'check balance']
        if (serviseAvailailabeList.indexOf(intent) < 0) return alert(`Permintaan anda untuk ${intent} tidak bisa kami layani`);
        visitor.intent = intent;
        bankQueueList.push(visitor);
        console.log(`Hai ${visitor.name}, anda berada di antrian ke-${bankQueueList.length}. Silahkan menunggu untuk melakukan ${intent}`);
    }
}

class Customer extends Person {
    constructor(name, address, accountBalance = 100000, intent) {
        super(name, address);
        this.accountBalance = accountBalance;
        this._intent = '';
        customerList.push(this);
    }

    set intent(value) {
        this._intent = value
    }

    get intent() {
        return this._intent
    }
}

class Teller extends Person {
    constructor(name, address) {
        super(name, address);
        // this.tellerQueue = [];
    }
    serve() {
        if (bankQueueList.length === 0) return alert(`Belum ada antrian di Bank`)
        let visitor = bankQueueList[0];
        let intent = bankQueueList[0].intent;
        Teller.precessVisitorIntent(visitor, intent);

        // Teller.withdraw(visitor, intent);
        // Teller.deposit(visitor, intent);
        // Teller.checkBalance(visitor, intent);
        // Teller.transfer(visitor, intent);
        bankQueueList.shift();
        visitor.intent = ""
    }

    static precessVisitorIntent(visitor, intent) {
        if (intent === "withdraw") return Teller.withdraw(visitor, intent);
        if (intent === "deposit") return Teller.deposit(visitor, intent);
        if (intent === "transfer") return Teller.transfer(visitor, intent);
        if (intent === "check balance") return Teller.checkBalance(visitor, intent);
        // ['withdraw', 'deposit', 'transfer', 'check balance']
    }

    static withdraw(visitor, intent) {
        if (intent !== 'withdraw') return
        let withdrawAmount = parseInt(prompt(`Hai ${visitor.name}, berapa jumlah uang yang ingin di tarik?`))

        if (visitor.accountBalance > withdrawAmount) {
            visitor.accountBalance -= withdrawAmount
            alert(`BANK INFO: ${this.name} telah melayani ${visitor.name} untuk melakukan ${intent} sebesar ${withdrawAmount} `);
            console.log(`BANK INFO: ${this.name} telah melayani ${visitor.name} untuk melakukan ${intent} sebesar ${withdrawAmount} `);
            return
        }

        console.log(`Saldo anda tidak mencukipi`)
    }

    static deposit(visitor, intent) {
        if (intent !== 'deposit') return
        let depositAmount = parseInt(prompt(`Hai ${visitor.name}, berapa jumlah uang yang ingin anda deposit?`))
        visitor.accountBalance += depositAmount
        alert(`BANK INFO: ${this.name} telah melayani ${visitor.name} untuk melakukan ${intent} sebesar ${depositAmount} `);
        console.log(`BANK INFO: ${this.name} telah melayani ${visitor.name} untuk melakukan ${intent} sebesar ${depositAmount} `);
    }

    static checkBalance(visitor, intent) {
        if (intent !== 'check balance') return
        console.log(`BANK INFO: Hai ${visitor.name}, saya ${this.name}, saldo anda saat ini adalah ${visitor.accountBalance}`)
        alert(`BANK INFO: Hai ${visitor.name}, saya ${this.name}, saldo anda saat ini adalah ${visitor.accountBalance}`)

    }

    static transfer(visitor, intent) {
        if (intent !== 'transfer') return
        let askingRecepientName = prompt(`Hai ${visitor.name}. Kepada siapa kamu ingin mengirim uang?`);
        let askingTransferAmount = parseInt(prompt(`Berapa jumlah uang yang ingin dikirim ke ${askingRecepientName}?`));

        if (visitor.accountBalance > askingTransferAmount) {
            let transferRecepient = customerList.find(item => item.name === askingRecepientName);
            if (!transferRecepient) return alert(`Nama penerima tidak terdaftar, Periksa huruf besar dan hurus kecilnya`)
            visitor.accountBalance -= askingTransferAmount;
            transferRecepient.accountBalance += askingTransferAmount;;
            console.log(`BANK INFO: ${this.name} telah melayani ${visitor.name} untuk melakukan ${intent} sebesar ${askingTransferAmount} `);
            alert(`BANK INFO: ${this.name} telah melayani ${visitor.name} untuk melakukan ${intent} sebesar ${askingTransferAmount} `);
            return
        }
        console.log(`Saldo anda tidak mencukipi`)

    }
}


let customerList = [];
let bankQueueList = [];

// Instantiate 2 Teller
let tellerA = new Teller('Dinda', 'Jambi');
let tellerB = new Teller('Devaryo', 'Papua');

// Instantiate 5 Teller
let lisa = new Customer('Lisa', "Pariaman", 500000);
let mona = new Customer('Mona', "Depok", 500000);
let putri = new Customer('Putri', "Jogja", 200000);
let jeje = new Customer('Jeje', "Jakarta", 220000);




Security.register(mona, 'withdraw');
Security.register(putri, 'transfer');
Security.register(lisa, 'deposit');
Security.register(jeje, 'check balance');


tellerB.serve();
tellerA.serve();
tellerA.serve();
tellerA.serve();




